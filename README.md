# search-bar

一个可展开折叠的 element-plus 风格的搜索组件，使用 vue3 + ts 实现。目前默认支持在 vue3 + ts 环境中使用。

> 如果想在 vue3 + js 的项目中使用，可以引入 /lib 文件中编译的 vue3-search-bar.common.js 文件，记得同时引入 vue3-search-bar.css

> 通常给搜索区域一个绝对定位的父元素，使得搜索组件脱离文档流，不影响页面下半部分的布局

### 安装

`npm install vue3-search-bar`

### 使用

- 全局注册

```ts
import { createApp } from 'vue'
import App from './App.vue'
import SearchBar from 'vue3-search-bar'

createApp(App)
  .use(SearchBar)
  .mount('#app')
```

- 按需引用

```xml
<template>
  <search-bar :search-config="searchConfig" @search="handleSearch">
    <el-button type="primary">右侧按钮</el-button>
  </search-bar>
</template>

<script lang="ts" setup>
import { onMounted } from 'vue'
import { SearchBar, ISearchConfig } from 'vue3-search-bar'

const searchConfig: ISearchConfig[] = [
  {
    label: '状态',
    field: 'stautus',
    type: 'select',
    defaultShow: true,
    options: [
      {
        label: '正在进行',
        value: 'running'
      },
      {
        label: '已完成',
        value: 'finished'
      }
    ],
    multiple: false
  },
  {
    label: '项目名称',
    field: 'name',
    defaultShow: true,
    type: 'input'
  },
  {
    label: '型号',
    field: 'model',
    type: 'select',
    options: []
  },
  {
    label: '是否修正',
    field: 'modified',
    type: 'switch',
    default: true
  },
  {
    label: '创建日期',
    field: 'createDate',
    type: 'datetimerange'
  },
  {
    label: '模态不变量',
    field: 'const',
    type: 'inputrange'
  }
]

function handleSearch () {
  console.log('search')
}

onMounted(async () => {
  // 模拟异步请求回来的数据
  setTimeout(() => {
    searchConfig.forEach(item => {
      if (item.field === 'model') {
        item.options = ['model1', 'model2'].map(item => ({ label: item, value: item }))
      }
    })
  }, 500)
})
</script>
```

- 如果需要 v-ellipsis 可以引入注册

v-ellipsis 是一个溢出隐藏时鼠标悬浮显示 title 的全局自定义指令

```ts
// 安装
import { createApp } from 'vue'
import App from './App.vue'
import { Ellipsis } from 'vue3-search-bar'

createApp(App)
  .directive('ellipsis', Ellipsis)
  .mount('#app')
```

```xml
<!-- 使用: 需要有确定的宽度，固定宽度或百分比宽度均可 -->
<div v-ellipsis style="width: 100%;">如果文字过长，会隐藏超出的部分并以...显示，鼠标悬浮会显示所有内容</div>
```

- 在js中使用
```js
import { createApp } from 'vue'
import App from './App.vue'
import SearchBar, { Ellipsis } from 'vue3-search-bar/lib/vue3-search-bar.common.js'
import 'vue3-search-bar/lib/vue3-search-bar.css'

createApp(App)
  .use(SearchBar)
  .directive('ellipsis', Ellipsis)
  .mount('#app')
```

## 开发
### Project setup
```
pnpm install
```

### Compiles and hot-reloads for development
```
pnpm run serve
```

### Compiles and minifies for production
```
pnpm run build
```

### Lints and fixes files
```
npm run lint
```

### 编译 js
```
npm run lib
```
