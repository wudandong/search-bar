export interface ISearchConfig {
  label: string
  type: 'input' | 'select' | 'inputrange' | 'daterange' | 'datetimerange' | 'switch'
  field: string // 字段名
  defaultShow?: boolean // 是否默认展示
  options?: Array<{label: string, value: string | boolean | number}> // select 的选项
  multiple?: boolean // 是否多选，用于 select
  default?: string | number | boolean | Array<string | number> // 默认值
  activeValue?: boolean | string | number // 开关激活状态值
  inactiveValue?: boolean | string | number // 开关关闭状态值
  activeText?: string // 开关激活状态显示文字
  inactiveText?: string // 开关关闭状态显示文字
  placeholder?: string
  width?: number // 布局的宽度，内部使用
}
