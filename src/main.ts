import { createApp } from 'vue'
import App from './App.vue'
import ellipsis from '@/directive/ellipsis'
import SearchBar from '@/components/search-bar'
import 'element-plus/dist/index.css'

createApp(App)
  .component('search-bar', SearchBar)
  .directive('ellipsis', ellipsis)
  .mount('#app')
