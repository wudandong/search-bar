import SearchBar from './components/search-bar'
import Ellipsis from './directive/ellipsis'
import { ISearchConfig } from './types'
import { App } from 'vue'

function install (app: App<Element>) {
  app.component('search-bar', SearchBar)
}

export {
  ISearchConfig,
  SearchBar,
  Ellipsis
}

export default {
  install
}
